


program: makefile-parser/program

makefile-parser/program:
	$(MAKE) -C makefile-parser

TEST_DIRS=$(wildcard tests/*.fakelib)
.PHONY: tests simple_tests $(TEST_DIRS)


tests: $(TEST_DIRS) run.sh program

$(TEST_DIRS): %.fakelib : %.sh.expected
	@./tests/test_runner.sh $@ $<
	@echo test $@ passed

