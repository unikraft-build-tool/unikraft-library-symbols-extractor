#!/bin/bash

# $1 Being the library folder

#need the environment variables declared by some rule translations
set -a
CLANGPARSER=$HOME/parserClang/parserClang.py

if test -f $1/exportsyms.uk; then
	./text-to-json/txt2json.py -i $1/exportsyms.uk -e -s $'\n' -r none | jq
	exit 0
fi

source rules_translation.sh

makefile=$1/Makefile.uk

BUILD_DIR=$PWD/build

addlibs=$(cat $makefile | ./makefile-parser/program | grep function_call | egrep 'addlib(_s)?')


for addlib in $(echo $addlibs | tr -d ' ' | tr '\n' ' '); do
	cmd=$(echo "$addlib" | sed 's/function_call://g')
	cmd=$(echo "$cmd" | sed -r 's/addlib_s(.*),/addlib\1/g') # addlib_s is mostly for configuration of an application so might as well get rid of it, remove the last comma as well
	# echo command: $cmd
	cmd=$(echo $cmd | tr ',' ' ')
	$cmd
done

fetches=$(cat $makefile | ./makefile-parser/program | grep function_call | egrep 'fetch(as)?' )


if test -z "$TEST"; then
	#function_call:fetch(as)?[, arg]{1,}
	for fetch in $(echo $fetches | tr -d ' ' | tr '\n' ' '); do
		cmd=$(echo "$fetch" | sed 's/function_call://g' | tr ',' ' ')
		$cmd
	done
fi
if test -n "$TEST"; then
	srcs_raw=$(cat $makefile | ./makefile-parser/program -n | egrep 'SRCS(-y)?')
else
	srcs_raw=$(cat $makefile | ./makefile-parser/program | egrep 'SRCS(-y)?')
fi

# It's double colons because it's empty at the beginning
# SOMETHINGS-SRCS(-y)?::[src:]{,}
srcs=$(echo "$srcs_raw" | sed -r 's/.*SRCS(-y)?:://g' | tr ':' ' ')


cd parserClang
full=''
if test -n "$TEST"; then
	for src in $srcs; do
		found=1
		for test_src in $TEST_SRCS; do
			if test "$test_src" == "$src"; then
				found=0
				break
			fi
		done
		if test "$found" == 1; then
			echo "TEST FAILED"
			echo "Didn't find $test_src"
			echo "found: $srcs"
			exit 1
		fi
	done
else
	./parserClang.py -q $srcs | jq ".functions | map(.name)"

fi
