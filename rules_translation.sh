#!/bin/bash


# Common Rules


set_build_dir() {
    #it's the parameter O in the unikraft Makefile
    #for my purposes $PWD/build should be good
    BUILD_DIR=$PWD/build
}

lc () {
    echo "$1" | tr '[:upper:]' '[:lower:]'
}

uc () {
    echo "$1" | tr '[:lower:]' '[:upper:]'
}

#UNDEFINED gcc_version_ge

#UNEDEFINED error_if_gcc_version_lt

################################################################################
#
# Paths and filenames
#
################################################################################

mk_sub_build_dir () {
    mkdir -p "$BUILD_DIR"/"$1"
    if test "$?" != 0; then
        echo "Could not create directory $BUILD_DIR/$1"
        false
    fi
}

fileext () {
    echo "$1" | sed -r 's/.*\.([a-zA-Z0-9])+/\1/g'
}

libname2lib () {
    local ret=""
    for name in $1; do
        ret="$ret $BUILD_DIR/$name.o"
    done
    echo $ret
}

libname2preolib () {
    local ret=""
    for name in $1; do
        ret="$ret $BUILD_DIR/$name.ld.o"
    done
    echo $ret
}

#UNDEFINED src2dst

#UNDEFINED src2obj

#UNDEFINED out2dep

#UNDEFINED src2dep

#UNDEFINED src2lds

#UNDEFINED dts2dtb

vprefix_lib () {
    echo "$(uc $1)_$2)"
}

#UNDEFINED vprefix_src

#UNDEFINED vprefix_glb

#UNDEFINED verbose_include

#UNDEFINED verbose_include_try

#UNDEFINED _import_lib

#UNDEFINED _import_linker

#UNDEFINED addplat

#UNDEFINED addplat_s

addlib () {
    mk_sub_build_dir $1
    local uc_var=$(uc $1)
    declare ${uc_var}_BASE=$_IMPORT_BASE # ???
    declare ${uc_var}_BUILD=$BUILD_DIR/$1
    #exports and locals are not useful to me, pretty sure
}

addlib_s () {
    if test "$2" = "y"; then
        addlib $1
    fi
}

#UNDEFINED addplatlib

#UNDEFINED addplatlib_s

################################################################################
#
# Commands
#
################################################################################

#UNDEFINED all


#################################################
#
# Archives
#
#################################################


fetchas () {
    wget -q --show-progress --progress=bar -O "$BUILD_DIR/$1/$3" "$2"
    unarchive "$1" "$BUILD_DIR/$1/$3" "$4"
}

notdir () {
	echo "$1" | sed -r 's/([^\/]*\/)*(.*)/\2/g'
}

fetch () {
	fetchas $1 $2 $(notdir $2) $3
}

unarchive () {
    case $2 in
        *.tar.gz*|*.tgz*)
            tar -xzf $2 -C $BUILD_DIR/$1/origin
            # touch $BUILD_DIR/$1/.origin
            ;;
        *.tar.bz2*|*.tbz2*)
            tar -xjf $2 -C $BUILD_DIR/$1/origin
            # touch $BUILD_DIR/$1/.origin
            ;;
        *.tar.xz*|.txz*)
            tar -xJf $2 -C $BUILD_DIR/$1/origin
            # touch $BUILD_DIR/$1/.origin
            ;;
        *.zip*)
            unzip -qq -u -d $BUILD_DIR/$1/origin $2
            # touch $BUILD_DIR/$1/.origin
            ;;
        *)
            false
            ;;
    esac
    declare -gx ${1^^}_ORIGIN=$BUILD_DIR/$1/origin
    mk_sub_build_dir $1/origin
}
